package com.example.hw12

import androidx.lifecycle.*

open class DataModel: ViewModel() {
    val current: MutableLiveData<Profile> by lazy{
        MutableLiveData<Profile>()
    }
    val data: MutableLiveData<Profile> by lazy{
        MutableLiveData<Profile>()
    }
}
