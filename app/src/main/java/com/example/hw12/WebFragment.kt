package com.example.hw12

import android.annotation.SuppressLint
import android.os.*
import android.view.*
import android.webkit.WebViewClient
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.hw12.databinding.FragmentWebBinding

class WebFragment : Fragment() {
    private lateinit var binding: FragmentWebBinding

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val callback = object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                if(binding.wbView.canGoBack()) binding.wbView.goBack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)
        binding = FragmentWebBinding.inflate(inflater)
        binding.bottomNavig.selectedItemId = R.id.web
        binding.bottomNavig.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.profile -> {
                    Navigation.findNavController(binding.root).navigate(R.id.profileFragmentNW)
                }
                R.id.web -> { }
                R.id.home -> {
                    Navigation.findNavController(binding.root).navigate(R.id.homeFragmentNW)
                }
            }
            true
        }
        webViewSetup()
        return binding.root
    }

    @SuppressLint("SetJavaScriptEnabled")
    @RequiresApi(Build.VERSION_CODES.O)
    private fun webViewSetup(){
        binding.wbView.webViewClient = WebViewClient()
        binding.wbView.apply {
            loadUrl("https://www.google.com/")
            settings.javaScriptEnabled = true
            settings.safeBrowsingEnabled = true
            var userUrl = url.toString()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = WebFragment()
    }
}

