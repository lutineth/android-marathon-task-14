package com.example.hw12

data class Profile(val login: String, val password: String, val name: String, val birthday: String)