package com.example.hw12

import android.os.Bundle
import android.view.*
import androidx.fragment.app.*
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.Navigation
import com.example.hw12.databinding.FragmentProfileBinding

class ProfileFragment : Fragment() {
    private val dataModel: DataModel by activityViewModels()
    private lateinit var binding: FragmentProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater)
        binding.bottomNavig.selectedItemId = R.id.web
        binding.bottomNavig.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.web -> {
                    Navigation.findNavController(binding.root).navigate(R.id.webFragmentNP)
                }
                R.id.profile -> { }
                R.id.home -> {
                    Navigation.findNavController(binding.root).navigate(R.id.homeFragmentNP)
                }
            }
            true
        }
        binding.btExit.setOnClickListener{
            Navigation.findNavController(binding.root).navigate(R.id.loginFragmentNP)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dataModel.current.observe(activity as LifecycleOwner) {
            binding.tvUserName.text = it.name
            binding.tvUserBDay.text = it.birthday
        }
    }
    companion object {

        @JvmStatic
        fun newInstance() = ProfileFragment()
    }
}
